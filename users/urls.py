# users/urls.py
from django.urls import path
from .views import UserViewSet

urlpatterns = [
    path('api/user/register/', UserViewSet.as_view({'post': 'register'}), name='register'),
    path('api/user/login/', UserViewSet.as_view({'post': 'login'}), name='login'),
    
]
