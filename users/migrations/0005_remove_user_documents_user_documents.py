# Generated by Django 5.0.1 on 2024-01-10 00:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0001_initial'),
        ('users', '0004_rename_customuser_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='documents',
        ),
        migrations.AddField(
            model_name='user',
            name='documents',
            field=models.ManyToManyField(blank=True, to='document.document'),
        ),
    ]
