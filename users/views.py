from django.http import JsonResponse
from rest_framework import viewsets
from users.models import User
from users.serializers import UserSerializer
from django.contrib.auth.hashers import make_password, check_password
from rest_framework.decorators import action
from django.contrib import messages
from rest_framework import status
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import RefreshToken

class UserViewSet(viewsets.ModelViewSet):
    queryset =User.objects.all()
    serializer_class = UserSerializer
    
    
    def generate_tokens(user):
      refresh = RefreshToken.for_user(User)

      return {
        'refresh_token': str(refresh),
        'access_token': str(refresh.access_token),
    }
    
    @action(detail=False, methods=['post'], url_path='register')
    def register(self, request):
        try:
            serializer = UserSerializer(data=request.data  
            )

            if serializer.is_valid():
                serializer.save()
                user_data = serializer.data
                del user_data['password']
                data = {
                    "status": True,
                    "data": user_data
                }
                return JsonResponse(data, status=201)
            
            data = {
                "status": False,
                "error": serializer.errors
            }
            return JsonResponse(data, status=400)

        except Exception as e:
            data = {
                "status": False,
                "error": str(e)
            }
            return JsonResponse(data, status=400)
    

    @action(detail=False, methods=['post'], url_path='login')
    def login(self, request):
        try:
            email = self.request.data.get('email')
            password = self.request.data.get('password')
            user = User.objects.filter(email=email).first()
        
            if user:
                if check_password(password, user.password):
                    refresh = RefreshToken.for_user(user)
                    data ={
                        "status":True,
                        "token":str(refresh.access_token),
                        "firstname":user.firstname,
                        "email":user.email,
                        
                    }
                    
                    print(type(True))


                    return JsonResponse (data, status=200)
                
                raise  ValueError("The password is not correct")
        
            raise  ValueError("The email is not valid")
        except Exception as e:
            data ={
                "status":False,
                "error":  str(e)
            }
            return JsonResponse (data, status =400)
