# Using a Base Image with Python
FROM python:3.11
# Set environment to non-interactive mode
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE=1
# Create and define the working directory in the container
WORKDIR /app

# Copy the requirements.txt file to the container
COPY requirements.txt /app/

# Install dependencies
RUN pip install --upgrade pip && pip install -r requirements.txt

# Copy the rest of the files to the container
COPY . /app/

# Expose the port on which the application will listen
EXPOSE 8000

# Run migrations (make sure to adjust this as needed)
RUN python manage.py migrate

# Enter command to start application (make sure to adjust this according to your needs)
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
