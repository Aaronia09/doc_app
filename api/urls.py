from rest_framework.routers import DefaultRouter
from document.views import  DocViewSet
from users.views import UserViewSet



router = DefaultRouter()

router.register(r'user', UserViewSet, basename='user')
router.register(r'document', DocViewSet, basename='document')

urlpatterns = [
   
]

urlpatterns += router.urls


