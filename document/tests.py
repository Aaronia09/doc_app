from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import Document, Annotation
from django.contrib.auth.models import User

class DocViewSetTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_document(self):
        # Creates a user for authentication
        user = User.objects.create_user(username='testuser', password='testpass')

        # Authenticates the user
        self.client.force_authenticate(user=user)

        # Sends a POST request to create a document
        response = self.client.post('/api/documents/', {'your_data_key': 'your_data_value'})

        # Checks if the creation was successful (HTTP status 201)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Document.objects.count(), 1)
       
class AnnotationViewSetTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_annotation(self):
        # Creates a user for authentication
        user = User.objects.create_user(username='testuser', password='testpass')

        # Creates a document associated with the user
        document = Document.objects.create(your_document_data_key='your_document_data_value', uploadedby=user)

        # Authentifie l'utilisateur
        self.client.force_authenticate(user=user)

        # Sends a POST request to create an annotation associated with the created document
        response = self.client.post('/api/annotations/', {'your_annotation_data_key': 'your_annotation_data_value', 'document': document.id})

        # Checks if the creation was successful (HTTP status 201)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Annotation.objects.count(), 1)
       

    def test_destroy_annotation(self):
         # Creates a user for authentication
        user = User.objects.create_user(username='testuser', password='testpass')

        # Creates a document associated with the user
        document = Document.objects.create(your_document_data_key='your_document_data_value', uploadedby=user)

        # Creates an annotation associated with the document
        annotation = Annotation.objects.create(your_annotation_data_key='your_annotation_data_value', document=document, annotedby=user)

        # Authenticates the user
        self.client.force_authenticate(user=user)

        # Sends DELETE request to delete annotation
        response = self.client.delete(f'/api/annotations/{annotation.id}/')

        # Checks if the deletion was successful (HTTP 204 status)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Annotation.objects.count(), 0)
