import uuid
from django.db import models
from django.contrib.auth.models import User


# Model for document management
class Document(models.Model):
    DOCS_CHOICES = (
        ('active', 'Active'),
        ('archived', 'Archived'),
        ('draft', 'Draft'),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    docfile = models.FileField(upload_to="documents")
    doc = models.CharField(max_length=25, choices=DOCS_CHOICES)
    date = models.DateField(auto_now_add=True)
    uploadedby = models.ForeignKey(User, on_delete=models.CASCADE)
    
  
 