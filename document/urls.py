
from django.urls import path
from .views import DocViewSet

urlpatterns = [
    path('api/document/', DocViewSet.as_view, name='document'),
    path('api/document/create', DocViewSet.as_view, name='document-create'),
    path('api/annotation/', DocViewSet.as_view, name='annotation'),
    
]
