from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .models import Document
from .serializers import  DocSerializer

class IsAnnotationOwnerOrAssociated(permissions.BasePermission):
    """
    Custom permission to allow the annotation owner or an associated user
    the same document to delete the annotation.
    """

    def has_object_permission(self, request, view, obj):
        # Check if the current user is the creator of the annotation
        if request.user == obj.annotedby:
            return True

       # Check if the current user is associated with the same document
        if request.user in obj.document.uploadedby.all():
            return True

        return False


class DocViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocSerializer

    def create(self, request, *args, **kwargs):
        serializer = DocSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


